import React, { Component } from 'react';
import Button from './Button';

class PrincipalCard extends Component {
  constructor () {
    super();
    this.state = {
      Carrera: 'Informática',
      Grado: 'Sexto Perito',
      Colegio: 'KINAL',
      Zona: 'Zona 7'
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.onAddTodo(this.state);
    this.setState({
      Carrera: 'Informática',
      Grado: 'Sexto Perito',
      Colegio: 'KINAL',
      Zona: 'Zona 7'
    });
  }

  handleInputChange(e) {
    const {value, name} = e.target;
    console.log(value, name);
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div className="card" >
        <form onSubmit={this.handleSubmit} className="card-body">
          <div className="form-group">
            <input
              type="text"
              name="Carrera"
              className="form-control"
              value={this.state.Carrera}
              onChange={this.handleInputChange}
              placeholder="Carrera"
              />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="Grado"
              className="form-control"
              value={this.state.Grado}
              onChange={this.handleInputChange}
              placeholder="Grado"
              />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="Colegio"
              className="form-control"
              value={this.state.Colegio}
              onChange={this.handleInputChange}
              placeholder="Colegio"
              />
          </div>
          <div className="form-group">
            <select
                name="Zona"
                className="form-control"
                value={this.state.Zona}
                onChange={this.handleInputChange}
              >
              <option>Seleccione la zona</option>
              <option>Zona 1</option>
              <option>Zona 2</option>
              <option>Zona 3</option>
              <option>Zona 4</option>
              <option>Zona 5</option>
              <option>Zona 6</option>
              <option>Zona 7</option>
              <option>Zona 8</option>
              <option>Zona 9</option>
              <option>Zona 10</option>
            </select>
          </div>
          <Button/>
        </form>
      </div>
    )
  }
}

export default PrincipalCard;
