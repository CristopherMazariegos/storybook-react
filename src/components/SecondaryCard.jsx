import React from 'react';

function SecondaryCard(props) {

      return (
        <div className="col-md-4" key={props.i}>
          <div className="card mt-4">
            <div className="card-title text-center">
              <h3>KINAL</h3>
              <span className="badge badge-pill badge-danger ml-2">
                Zona 7
              </span>
            </div>
            <div className="card-body">
            <input
              type="text"
              name="Grado"
              className="form-control"
              value= "Sexto Perito"
              />
              <br></br>
              <input
              type="text"
              name="Carrera"
              className="form-control"
              value= "Informatica"
              />            
            </div>
            <div className="card-footer">
              <button
                className="btn btn-danger"
                onClick={()=>{
                  props.removeTodo(props.i)
                }}>
                Eliminar
              </button>
            </div>
          </div>
        </div>
      )
    };

export default SecondaryCard;
