import React, { Component } from 'react';
import logo from './KINAL.png';
import './App.css';

// data
import { todos } from './todos.json';

// subcomponents
import PrincipalCard from './components/PrincipalCard.jsx';
import SecondaryCard from './components/SecondaryCard.jsx';

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos
    }
    this.handleAddTodo = this.handleAddTodo.bind(this);
    this.removeTodo=this.removeTodo.bind(this);
  }

  removeTodo(index) {
    this.setState({
      todos: this.state.todos.filter((e, i) => {
        return i !== index
      })
    });
  }

  handleAddTodo(todo) {
    this.setState({
      todos: [...this.state.todos, todo]
    })
  }

  render() {
    const todos = this.state.todos.map((todo, i) => {
      return(
        <SecondaryCard todo={todo} removeTodo={this.removeTodo} i={i}/>
      )
    });

    return (
      <div className="App">
        <nav className="navbar navbar">
          <a className="navbar-brand">
            KINAL
          </a>
        </nav>
          <div className="container">
            <div className="row mt-4">
              <div>
               <div className="col-md-4 text-center">
                <img src={logo} className="App-logo" alt="logo" />  
              </div>
              <br></br>
                <div>  
                  <PrincipalCard onAddTodo={this.handleAddTodo}></PrincipalCard>
                </div>
              </div>
            <div className="col-md-8">
              <div className="row">
                {todos}
              </div>
            </div>
            </div>
          </div>
      </div>
    );
  }
}

export default App;